/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.corriteca.service;

import com.corriteca.model.Aposta;
import com.corriteca.repository.ApostaCustomRepository;
import com.corriteca.repository.ApostaRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Ríllary Ramos
 */
@Service
public class ApostaService {
    
    @Autowired
    private ApostaRepository repository;
    
    @Autowired
    private ApostaCustomRepository customRepository;
    
    public List<Aposta> listar(){
        return repository.findAll();
    }
    
    public void salvar(Aposta aposta){
        repository.save(aposta);
    }
    
    public Aposta buscarPorId(Long id){
        return repository.getOne(id);
    }
    
    public void excluir(Long id){
//        repository.delete(id);
      repository.deleteById(id);
    }
    
    public void inativarAposta(Long idAposta){
        repository.inativarAposta(idAposta);
    }
    
    public Object valorArrecadado(){
        return customRepository.valorArrecadado();
    }
    
    public List<Aposta> pesquisarAposta(String tipoBusca, int valorTipoBusca){
        //valores: 1-Apostador; 2-Bloco; 3(default)-Cartela
        List<Aposta> aposta;
        switch(valorTipoBusca){
            case 1:
                aposta = repository.pesquisarApostaPorApostador(tipoBusca);
                break;
            case 2:
                aposta = repository.pesquisarApostaPorBloco(tipoBusca);
                break;
            default:
                aposta = repository.pesquisarApostaPorCartela(tipoBusca);
                break;
        }
        return aposta;
    }
}
