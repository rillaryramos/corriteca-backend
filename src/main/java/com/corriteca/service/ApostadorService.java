/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.corriteca.service;

import com.corriteca.model.Apostador;
import com.corriteca.repository.ApostadorRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Ríllary Ramos
 */
@Service
public class ApostadorService {
    
    @Autowired
    private ApostadorRepository repository;
    
    public List<Apostador> listar(){
        return repository.findAll();
    }
    
    public void salvar(Apostador apostador){
        repository.save(apostador);
    }
    
    public Apostador buscarPorId(Long id){
        return repository.getOne(id);
    }
    
    public void excluir(Long id){
//        repository.delete(id);
                repository.deleteById(id);
    }
}
