/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.corriteca.service;

import com.corriteca.model.Dupla;
import com.corriteca.repository.DuplaRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Ríllary Ramos
 */
@Service
public class DuplaService {
    
    @Autowired
    private DuplaRepository repository;
    
    public List<Dupla> listar(){
        return repository.findAll();
    }
    
    public void salvar(Dupla dupla){
        repository.save(dupla);
    }
    
    public Dupla buscarPorId(Long id){
        return repository.getOne(id);
    }
    
    public void excluir(Long id){
//        repository.delete(id);
                repository.deleteById(id);
    }
    
    public void editar(Dupla dupla){
        repository.save(dupla);
    }
}
