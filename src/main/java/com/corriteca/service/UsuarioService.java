/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.corriteca.service;

import com.corriteca.model.Usuario;
import com.corriteca.repository.UsuarioRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Ríllary Ramos
 */
@Service
public class UsuarioService {
    
    @Autowired
    private UsuarioRepository repository;
    
    public List<Usuario> listar(){
        return repository.findAll();
    }
    
    public void salvar(Usuario usuario){
        repository.save(usuario);
    }
    
    public Usuario buscarPorId(Long id){
        return repository.getOne(id);
    }
    
    public void excluir(Long id){
//        repository.delete(id);
                repository.deleteById(id);
    }
    
    public List<Usuario> pesquisarUsuario(String tipoBusca, int valorTipoBusca){
        //valores: 1-Nome; 2-Conferencia; 3(default)-Id
        List<Usuario> usuario;
        switch(valorTipoBusca){
            case 1:
                usuario = repository.pesquisarUsuarioPorNome(tipoBusca);
                break;
            case 2:
                usuario = repository.pesquisarUsuarioPorConferencia(tipoBusca);
                break;
            default:
                usuario = repository.pesquisarUsuarioPorId(tipoBusca);
                break;
        }
        return usuario;
    }
}
