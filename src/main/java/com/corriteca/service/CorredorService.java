/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.corriteca.service;

import com.corriteca.model.Corredor;
import com.corriteca.repository.CorredorRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Ríllary Ramos
 */
@Service
public class CorredorService {
    
    @Autowired
    private CorredorRepository repository;
    
    public List<Corredor> listar(){
        return repository.findAll();
    }
    
    public void salvar(Corredor corredor){
        repository.save(corredor);
    }
    
    public Corredor buscarPorId(Long id){
        return repository.getOne(id);
    }
    
    public void excluir(Long id){
//        repository.delete(id);
                repository.deleteById(id);
    }
    
    public List<Corredor> pesquisarCorredor(String tipoBusca, int valorTipoBusca){
        //valores: 1-Nome; 2(default)-Id
        List<Corredor> corredor;
        switch(valorTipoBusca){
            case 1:
                corredor = repository.pesquisarCorredorPorNome(tipoBusca);
                break;
            default:
                corredor = repository.pesquisarCorredorPorId(tipoBusca);
                break;
        }
        return corredor;
    }
    
    public Boolean verificarCorredorEmDupla(int idCorredor){
        if(repository.verificarCorredorEmDupla(idCorredor, idCorredor).isEmpty()){
            return false;
        }else{
            return true;
        }
    }
}
