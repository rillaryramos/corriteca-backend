/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.corriteca.service;

import com.corriteca.model.Conferencia;
import com.corriteca.repository.ConferenciaRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Ríllary Ramos
 */
@Service
public class ConferenciaService {
    
    @Autowired
    private ConferenciaRepository repository;
    
    public List<Conferencia> listar(){
        return repository.findAll();
    }
    
    public void salvar(Conferencia conferencia){
        repository.save(conferencia);
    }
    
    public Conferencia buscarPorId(Long id){
        return repository.getOne(id);
    }
    
    public void excluir(Long id){
//        repository.delete(id);
                repository.deleteById(id);
    }
}
