/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.corriteca.service;

import com.corriteca.model.Resultado;
import com.corriteca.repository.ResultadoRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Ríllary Ramos
 */
@Service
public class ResultadoService {
    
    @Autowired
    private ResultadoRepository repository;
    
    public void salvar(List<Resultado> resultado){
//        repository.save(resultado);
                repository.saveAll(resultado);
    }
    
    public List<Resultado> listar(){
        return repository.findAll();
    }
    
    public void excluirResultado(){
        repository.deleteAll();
    }
    
}
