/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.corriteca.controller;

import com.corriteca.model.Usuario;
import com.corriteca.service.UsuarioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Ríllary Ramos
 */
@RestController
@RequestMapping("api/usuarios")
public class UsuarioController {
    
    @Autowired
    private UsuarioService service;
    
    @PostMapping
//    @PreAuthorize("hasAuthority('ROLE_CADASTRAR_USUARIO')")
    public void salvar(@RequestBody Usuario usuario){
        service.salvar(usuario);
    }
    
    @GetMapping
//    @PreAuthorize("hasAuthority('ROLE_VISUALIZAR_USUARIOS')")
    public List<Usuario> listar(){
        return service.listar();
    }
    
    @GetMapping("/{id}")
    public Usuario buscarPorId(@PathVariable("id") Long id){
        return service.buscarPorId(id);
    }
    
    @PutMapping
    public void editar(@RequestBody Usuario usuario){
        service.salvar(usuario);
    }
    
    @DeleteMapping("/{id}")
    public void excluir(@PathVariable("id") Long id){
        service.excluir(id);
    }
    
    @GetMapping("/{valorTipoBusca}/{tipoBusca}")
    public List<Usuario> pesquisarUsuario(@PathVariable("valorTipoBusca") int valorTipoBusca, @PathVariable("tipoBusca") String tipoBusca) {
        return service.pesquisarUsuario(tipoBusca, valorTipoBusca);
    }
}
