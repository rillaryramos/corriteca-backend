/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.corriteca.controller;

import com.corriteca.model.Configuracao;
import com.corriteca.repository.ConfiguracaoRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Ríllary Ramos
 */
@RestController
@RequestMapping("api/configuracao")
public class ConfiguracaoController {
    
    @Autowired
    private ConfiguracaoRepository repository;
    
    @GetMapping
    public List<Configuracao> listar(){
        return repository.findAll();
    }
    
    @PutMapping
    public void editar(@RequestBody Configuracao config){
        repository.save(config);
    }
}
