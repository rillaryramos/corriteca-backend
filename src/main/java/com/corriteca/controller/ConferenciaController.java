/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.corriteca.controller;

import com.corriteca.model.Conferencia;
import com.corriteca.service.ConferenciaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Ríllary Ramos
 */
@RestController
@RequestMapping("api/conferencias")
public class ConferenciaController {
    
    @Autowired
    private ConferenciaService service;
    
    @PostMapping
    public void salvar(@RequestBody Conferencia conferencia){
        service.salvar(conferencia);
    }
    
    @GetMapping
    public List<Conferencia> listar(){
        return service.listar();
    }
    
    @GetMapping("/{id}")
    public Conferencia buscarPorId(@PathVariable("id") Long id){
        return service.buscarPorId(id);
    }
    
    @PutMapping
    public void editar(@RequestBody Conferencia conferencia){
        service.salvar(conferencia);
    }
    
    @DeleteMapping("/{id}")
    public void excluir(@PathVariable("id") Long id){
        service.excluir(id);
    }
    
}
