/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.corriteca.controller;

import com.corriteca.model.Resultado;
import com.corriteca.service.ResultadoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Ríllary Ramos
 */
@RestController
@RequestMapping("api/resultado")
public class ResultadoController {
    
    @Autowired
    private ResultadoService service;
    
    @PostMapping
    public void salvar(@RequestBody List<Resultado> resultado){
        service.salvar(resultado);
    }
    
    @GetMapping
    public List<Resultado> listar(){
        return service.listar();
    }
    
    @DeleteMapping("/excluirResultado")
    public void excluirResultado(){
        service.excluirResultado();
    }
    
}
