/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.corriteca.controller;

import com.corriteca.model.Corredor;
import com.corriteca.service.CorredorService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Ríllary Ramos
 */

@RestController
@RequestMapping("api/corredores")
public class CorredorController {
    
    @Autowired
    private CorredorService service;
    
    @PostMapping
    public void salvar(@RequestBody Corredor corredor){
        service.salvar(corredor);
    }
    
    @GetMapping
    public List<Corredor> listar(){
        return service.listar();
    }
    
    @GetMapping("/{id}")
    public Corredor buscarPorId(@PathVariable("id") Long id){
        return service.buscarPorId(id);
    }
    
    @PutMapping
    public void editar(@RequestBody Corredor corredor){
        service.salvar(corredor);
    }
    
    @DeleteMapping("/{id}")
    public void excluir(@PathVariable("id") Long id){
        service.excluir(id);
    }
    
    @GetMapping("/{valorTipoBusca}/{tipoBusca}")
    public List<Corredor> pesquisarCorredor(@PathVariable("valorTipoBusca") int valorTipoBusca, @PathVariable("tipoBusca") String tipoBusca) {
        return service.pesquisarCorredor(tipoBusca, valorTipoBusca);
    }
    
    @GetMapping("/verificarCorredor/{id}")
    public Boolean verificarCorredorEmDupla(@PathVariable("id") int id){
        return service.verificarCorredorEmDupla(id);
    }
}
