/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.corriteca.controller;

import com.corriteca.model.PontuacaoApostadores;
import com.corriteca.repository.PontuacaoApostadoresRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Ríllary Ramos
 */
@RestController
@RequestMapping("api/pontuacao")
public class PontuacaoApostadoresController {
    
    @Autowired
    private PontuacaoApostadoresRepository repository;
    
    @GetMapping
    public List<PontuacaoApostadores> listar(){
        return repository.findAll();
    }
}
