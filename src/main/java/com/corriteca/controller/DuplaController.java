/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.corriteca.controller;

import com.corriteca.model.Dupla;
import com.corriteca.service.DuplaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Ríllary Ramos
 */
@RestController
@RequestMapping("api/duplas")
public class DuplaController {
    
    @Autowired
    private DuplaService service;
    
    @PostMapping
    public void salvar(@RequestBody Dupla dupla){
        service.salvar(dupla);
    }
    
    @GetMapping
    public List<Dupla> listar(){
        return service.listar();
    }
    
    @GetMapping("/{id}")
    public Dupla buscarPorId(@PathVariable("id") Long id){
        return service.buscarPorId(id);
    }
    
    @PutMapping
    public void editar(@RequestBody Dupla dupla){
        service.editar(dupla);
    }
    
    @DeleteMapping("/{id}")
    public void excluir(@PathVariable("id") Long id){
        service.excluir(id);
    }
}
