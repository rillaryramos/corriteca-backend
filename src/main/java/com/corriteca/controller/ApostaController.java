/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.corriteca.controller;

import com.corriteca.model.Aposta;
import com.corriteca.service.ApostaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Ríllary Ramos
 */
@RestController
@RequestMapping("api/apostas")
public class ApostaController {
    
    @Autowired
    private ApostaService service;
    
    
    @PostMapping
    public void salvar(@RequestBody Aposta aposta){
        service.salvar(aposta);
    }
    
    @GetMapping
    public List<Aposta> listar(){
        return service.listar();
    }
    
    @GetMapping("/{id}")
    public Aposta buscarPorId(@PathVariable("id") Long id){
        return service.buscarPorId(id);
    }
    
    @PutMapping
    public void editar(@RequestBody Aposta aposta){
        service.salvar(aposta);
    }
    
    @DeleteMapping("/{id}")
    public void excluir(@PathVariable("id") Long id){
        service.excluir(id);
    }
    
    @GetMapping("/inativarAposta/{id}")
    public void inativarAposta(@PathVariable("id") Long id){
        service.inativarAposta(id);
    }
    
    @GetMapping("/valorArrecadado")
    public Object quantidadeApostasValidas(){
        return service.valorArrecadado();
    }
    
    @GetMapping("/{valorTipoBusca}/{tipoBusca}")
    public List<Aposta> pesquisarAposta(@PathVariable("valorTipoBusca") int valorTipoBusca, @PathVariable("tipoBusca") String tipoBusca) {
        return service.pesquisarAposta(tipoBusca, valorTipoBusca);
    }
}
