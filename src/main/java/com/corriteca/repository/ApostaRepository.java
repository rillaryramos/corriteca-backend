/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.corriteca.repository;

import com.corriteca.model.Aposta;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Ríllary Ramos
 */
@Repository
public interface ApostaRepository extends JpaRepository<Aposta, Long>{
    
    @Modifying
    @Transactional
    @Query(value = "update aposta a set a.status = false where a.id = ?", nativeQuery = true)
    int inativarAposta(Long id);
    
    @Modifying
    @Query(value = "select count(*) from aposta as a where a.status = 1", nativeQuery = true)
    int quantidadeApostasValidas();
    
    @Modifying
    @Query(value = "select * from aposta as a join apostador as ap join pessoa as p on a.apostador_id = ap.id and ap.pessoa_id = p.id and p.nome like '%?%'", nativeQuery = true)
    List<Aposta> pesquisarApostaPorApostador(String nome);
    
    @Modifying
    @Query(value = "select * from aposta as a where a.numero_bloco = ?", nativeQuery = true)
    List<Aposta> pesquisarApostaPorBloco(String bloco);
    
    @Modifying
    @Query(value = "select * from aposta as a where a.numero_cartela = ?", nativeQuery = true)
    List<Aposta> pesquisarApostaPorCartela(String bloco);
}
