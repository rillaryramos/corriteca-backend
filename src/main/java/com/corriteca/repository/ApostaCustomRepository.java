/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.corriteca.repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Ríllary Ramos
 */
@Repository
public class ApostaCustomRepository {
    
    @Autowired
    private EntityManager entityManager;
 
    @Transactional
    public Object valorArrecadado(){
        Query query = entityManager.createNativeQuery("select (count(*)*5) from aposta as a where a.status = 1");
        return query.getSingleResult();
    }
}
