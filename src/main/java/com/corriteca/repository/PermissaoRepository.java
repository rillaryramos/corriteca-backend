/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.corriteca.repository;

import com.corriteca.model.Permissao;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author Ubion
 */
public interface PermissaoRepository extends JpaRepository<Permissao, Long> {

    @Query(value = "SELECT *  FROM usuario_permissoes up "
            + "JOIN usuario u ON u.id = up.usuario_id "
            + "JOIN permissao p ON p.id = up.permissoes_id "
            + "WHERE u.login = ?", nativeQuery = true)
    List<Permissao> buscarPermissoesPorUsuario(String login);
}
