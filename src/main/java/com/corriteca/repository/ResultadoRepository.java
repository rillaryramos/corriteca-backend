/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.corriteca.repository;

import com.corriteca.model.Resultado;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Ríllary Ramos
 */
@Repository
public interface ResultadoRepository extends JpaRepository<Resultado, Long>{
    
}
