/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.corriteca.repository;

import com.corriteca.model.Corredor;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Ríllary Ramos
 */
@Repository
public interface CorredorRepository extends JpaRepository<Corredor, Long>{
    
//  QUERYS PARA PESQUISAR PESSOA FÍSICA PELO NOME, CPF E ID
    
    @Modifying
    @Query(value = "select * from corredor as c join pessoa as p where c.pessoa_id = p.id and p.nome like %?%", nativeQuery = true)
    List<Corredor> pesquisarCorredorPorNome(String nome);
    
    @Modifying
    @Query(value = "select * from corredor as c where c.id = ?", nativeQuery = true)
    List<Corredor> pesquisarCorredorPorId(String id);
    
    @Modifying
    @Query(value = "select * from corredor as c join dupla as d on (? = d.corredor1_id) || (? = d.corredor2_id)", nativeQuery = true)
    List<Corredor> verificarCorredorEmDupla(int id, int id2);
    
}
