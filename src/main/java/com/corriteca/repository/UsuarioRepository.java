/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.corriteca.repository;

import com.corriteca.model.Usuario;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Ríllary Ramos
 */
@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

    Usuario findByLogin(String login);

//  QUERYS PARA PESQUISAR PESSOA FÍSICA PELO NOME, CPF E ID
    @Modifying
    @Query(value = "select * from usuario as u join pessoa as p where u.pessoa_id = p.id and p.nome like %?%", nativeQuery = true)
    List<Usuario> pesquisarUsuarioPorNome(String nome);

    @Modifying
    @Query(value = "select * from usuario as u where u.nome_conferencia like %?%", nativeQuery = true)
    List<Usuario> pesquisarUsuarioPorConferencia(String nomeConferencia);

    @Modifying
    @Query(value = "select * from usuario as u where u.id = ?", nativeQuery = true)
    List<Usuario> pesquisarUsuarioPorId(String id);

}
