/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.corriteca.security;

import com.corriteca.model.Pessoa;
import java.util.ArrayList;
import java.util.Collection;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 *
 * @author Ubion
 */
public class CustomUserDetails implements UserDetails {

    private Long id;
    private String login;
    private String senha;
    private Pessoa pessoa;
    private boolean ativo;
    private Collection<GrantedAuthority> permissoes = new ArrayList<>();

    public CustomUserDetails(Long id, Pessoa pessoa, String login, String senha, boolean ativo, Collection<GrantedAuthority> permissoes) {
        this.id = id;
        this.login = login;
        this.senha = senha;
        this.ativo = ativo;
        this.pessoa = pessoa;
        this.permissoes = permissoes;
    }

    public CustomUserDetails() {
    }
    
    

    public Long getId() {
        return id;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return permissoes;
    }

    @Override
    public String getPassword() {
        return senha;
    }

    @Override
    public String getUsername() {
        return login;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return ativo;
    }

    public Collection<GrantedAuthority> getPermissoes() {
        return permissoes;
    }

    public String getLogin() {
        return login;
    }

    public String getSenha() {
        return senha;
    }
    
    
}
