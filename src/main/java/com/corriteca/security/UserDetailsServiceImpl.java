/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.corriteca.security;

import com.corriteca.model.Permissao;
import com.corriteca.model.Usuario;
import com.corriteca.repository.PermissaoRepository;
import com.corriteca.repository.UsuarioRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 *
 * @author Ubion
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UsuarioRepository repository;

    @Autowired
    private PermissaoRepository permissaoRepository;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        System.out.println(">>>>>>>>>>>>>>>> "+ login);
        Usuario usuario = repository.findByLogin(login);

        if (usuario == null) {
            throw new UsernameNotFoundException("Usuário " + login + " não encontrado!");
        }
        List<GrantedAuthority> permissoes = permissoes(login);

        return new CustomUserDetails(usuario.getId(), usuario.getPessoa(), usuario.getLogin(), usuario.getSenha(), usuario.isAtivo(), permissoes);
    }

    public List<GrantedAuthority> permissoes(String login) {
        List<Permissao> permissoesPorUsuario = permissaoRepository.buscarPermissoesPorUsuario(login);
        List<GrantedAuthority> permissoes = new ArrayList<>();
        for (Permissao perm : permissoesPorUsuario) {
            permissoes.add(new SimpleGrantedAuthority(perm.getNome()));
        }
        return permissoes;
    }
}
