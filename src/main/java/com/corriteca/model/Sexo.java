/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.corriteca.model;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 *
 * @author Ríllary Ramos
 */

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum Sexo {
    
    MASCULINO("M", "MASCULINO"),
    FEMININO("F", "FEMININO");

    private final String sigla;
    private final String descricao;

    Sexo(String sigla, String descricao) {
        this.sigla = sigla;
        this.descricao = descricao;
    }

    public String getSigla() {
        return sigla;
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return this.descricao;
    }
    
}
