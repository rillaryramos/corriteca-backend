/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.corriteca.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author Ríllary Ramos
 */
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Entity
public class Configuracao implements Serializable{
        
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    private Boolean apostasLiberadas;
    
    private int qtdAlteracao;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getApostasLiberadas() {
        return apostasLiberadas;
    }

    public void setApostasLiberadas(Boolean apostasLiberadas) {
        this.apostasLiberadas = apostasLiberadas;
    }

    public int getQtdAlteracao() {
        return qtdAlteracao;
    }

    public void setQtdAlteracao(int qtdAlteracao) {
        this.qtdAlteracao = qtdAlteracao;
    }
    
}
