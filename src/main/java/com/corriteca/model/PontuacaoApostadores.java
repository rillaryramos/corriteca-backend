/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.corriteca.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author Ríllary Ramos
 */
@Entity
public class PontuacaoApostadores implements Serializable  {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    private String nomeApostador;
    private String telefoneApostador;
    private String emailApostador;
    private Long numeroBloco;
    private Long numeroCartela;
    private String nomeVendedor;
    private Long quantidadeAcertos;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomeApostador() {
        return nomeApostador;
    }

    public void setNomeApostador(String nomeApostador) {
        this.nomeApostador = nomeApostador;
    }

    public String getTelefoneApostador() {
        return telefoneApostador;
    }

    public void setTelefoneApostador(String telefoneApostador) {
        this.telefoneApostador = telefoneApostador;
    }

    public String getEmailApostador() {
        return emailApostador;
    }

    public void setEmailApostador(String emailApostador) {
        this.emailApostador = emailApostador;
    }

    public Long getNumeroBloco() {
        return numeroBloco;
    }

    public void setNumeroBloco(Long numeroBloco) {
        this.numeroBloco = numeroBloco;
    }

    public Long getNumeroCartela() {
        return numeroCartela;
    }

    public void setNumeroCartela(Long numeroCartela) {
        this.numeroCartela = numeroCartela;
    }

    public String getNomeVendedor() {
        return nomeVendedor;
    }

    public void setNomeVendedor(String nomeVendedor) {
        this.nomeVendedor = nomeVendedor;
    }

    public Long getQuantidadeAcertos() {
        return quantidadeAcertos;
    }

    public void setQuantidadeAcertos(Long quantidadeAcertos) {
        this.quantidadeAcertos = quantidadeAcertos;
    }
    
}
