/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.corriteca.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 *
 * @author Ríllary Ramos
 */
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Entity
public class Dupla implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @OneToOne
    private Corredor corredor1;
    
    @OneToOne
    private Corredor corredor2;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Corredor getCorredor1() {
        return corredor1;
    }

    public void setCorredor1(Corredor corredor1) {
        this.corredor1 = corredor1;
    }

    public Corredor getCorredor2() {
        return corredor2;
    }

    public void setCorredor2(Corredor corredor2) {
        this.corredor2 = corredor2;
    }
    
}
